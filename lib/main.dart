import 'package:flutter/material.dart';
import 'package:flutter_task1/ui/login_screen.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_task1/ui/splash_screen.dart';
import 'generated/l10n.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      localizationsDelegates: const[
        S.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      locale: const Locale('ru', 'Ru'),
      supportedLocales: S.delegate.supportedLocales,
      home: const SplashScreen(),
    );
  }
}
