import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_task1/constants/app_assets.dart';
import 'package:flutter_task1/constants/app_colors.dart';
import 'package:flutter_task1/generated/l10n.dart';
import 'package:flutter_task1/ui/character_list.dart';
import 'package:flutter_task1/ui/settings_screen.dart';

class BottomNavBar extends StatelessWidget {
  final int current;

  const BottomNavBar({Key? key, required this.current}) : super(key: key);

  PageRouteBuilder _createRoute(Widget screen) {
    return PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) => screen,
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        return child;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      onTap: (index) {
        if (index == 0) {
          Navigator.of(context).pushAndRemoveUntil(
            _createRoute(const CharacterList()),
            (route) => false,
          );
        }
        if (index == 1) {
          Navigator.of(context).pushAndRemoveUntil(
            _createRoute(const SettingsScreen()),
            (route) => false,
          );
        }
      },
      currentIndex: current,
      selectedItemColor: AppColors.button,
      unselectedItemColor: AppColors.neutral2,
      items: [
        BottomNavigationBarItem(
          icon: SvgPicture.asset(
            AppAssets.svg.characters,
            color: current == 0 ? null : AppColors.neutral2,
          ),
          label: S.of(context).characters,
        ),
        BottomNavigationBarItem(
          icon: SvgPicture.asset(
            AppAssets.svg.settings,
            color: current == 0 ? null : AppColors.button,
          ),
          label: S.of(context).settings,
        )
      ],
    );
  }
}
