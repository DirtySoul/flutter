import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_task1/constants/app_assets.dart';
import 'package:flutter_task1/constants/app_colors.dart';
import 'package:flutter_task1/constants/app_styles.dart';
import 'package:flutter_task1/generated/l10n.dart';

class PasswordField extends StatelessWidget {
  const PasswordField({Key? key, this.onSaved}) : super(key: key);

  final Function(String?)? onSaved;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      style: AppStyles.s16w400.copyWith(
        color: AppColors.mainText,
      ),
      decoration: InputDecoration(
        border: InputBorder.none,
        prefixIcon: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: SvgPicture.asset(
            AppAssets.svg.account,
            width: 16,
            color: AppColors.neutral2,
          ),
        ),
        hintText: S.of(context).password,
        hintStyle: AppStyles.s16w400.copyWith(
          color: AppColors.neutral2,
        ),
        counterText: '',
      ),
      maxLength: 16,
      obscureText: true,
      validator: (value){
        if (value == null) return S.of(context).passwordErrorCheck;
        if (value.length < 3) {
          return S.of(context).inputErrorPasswordIsShort;
        }
        return null;
      },
      onSaved: onSaved,
    );
  }
}
