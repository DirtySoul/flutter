import 'package:flutter/material.dart';
import 'package:flutter_task1/ui/character_list.dart';
import 'package:flutter_task1/constants/app_assets.dart';
import 'package:flutter_task1/constants/app_colors.dart';
import 'package:flutter_task1/constants/app_styles.dart';
import 'package:flutter_task1/ui/login/login_field.dart';
import 'package:flutter_task1/ui/login/password_field.dart';
import '../generated/l10n.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() {
    return _LoginScreenState();
  }
}

class _LoginScreenState extends State<LoginScreen> {
  final formKey = GlobalKey<FormState>();

  String? password;
  String? login;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          SizedBox(height: MediaQuery.of(context).viewPadding.top + 10),
          Expanded(child: Image.asset(AppAssets.images.logo),),
          Form(
            key: formKey,
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 12.0),
                    child: Text(
                      S.of(context).login,
                      style: AppStyles.s16w400.copyWith(
                        color: AppColors.mainText,
                      ),
                    ),
                  ),
                  Container(
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(12.0)),
                      color: AppColors.neutral1,
                    ),
                    child: LoginField(
                      onSaved: (login) {
                        this.login = login;
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 12.0),
                    child: Text(
                      S.of(context).password,
                      style: AppStyles.s16w400.copyWith(
                        color: AppColors.mainText,
                      ),
                    ),
                  ),
                  Container(
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(12.0)),
                      color: AppColors.neutral1,
                    ),
                    child: PasswordField(onSaved: (password) {
                      this.password = password;
                    }),
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Row(
              children: [
                Expanded(
                  child: TextButton(
                    style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all<Color>(AppColors.button),
                    ),
                    child: Text(
                      S.of(context).signIn,
                      style: AppStyles.s16w400.copyWith(color: Colors.white),
                    ),
                    onPressed: () {
                      if (formKey.currentState?.validate() ?? false) {
                        FocusScope.of(context).unfocus();
                        formKey.currentState?.save();
                        if (login == 'qwerty' && password == '123456ab') {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => const CharacterList(),
                            ),
                          );
                        } else {
                          showDialog<String>(
                            context: context,
                            builder: (BuildContext context) => AlertDialog(
                              actionsAlignment: MainAxisAlignment.center,
                              title: Text(S.of(context).error),
                              content: Text(S.of(context).wrongLoginOrPassword),
                              actions: [
                                OutlinedButton(
                                  onPressed: () => Navigator.pop(context),
                                  child: Text(
                                    S.of(context).close,
                                    style: AppStyles.s16w400
                                        .copyWith(color: AppColors.button),
                                  ),
                                  style: ButtonStyle(
                                    side: MaterialStateProperty.all(
                                      const BorderSide(color: AppColors.button),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          );
                        }
                      }
                    },
                  ),
                ),
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                S.of(context).noAccount,
                style: AppStyles.s16w400.copyWith(color: AppColors.neutral2),
              ),
              TextButton(
                onPressed: () {},
                child: Text(
                  S.of(context).createAccount,
                  style: AppStyles.s16w400.copyWith(color: AppColors.alive),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
