class Character {
  const Character({
    this.id,
    this.name,
    this.status,
    this.species,
    this.gender,
    this.origin,
    this.location,
    this.image,
  });

  final int? id;
  final String? name;
  final String? status;
  final String? species;
  final String? gender;
  final String? origin;
  final String? location;
  final String? image;
}
