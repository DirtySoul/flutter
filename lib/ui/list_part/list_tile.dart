import 'package:flutter/material.dart';
import 'package:flutter_task1/constants/app_assets.dart';
import 'package:flutter_task1/constants/app_styles.dart';
import 'package:flutter_task1/constants/app_colors.dart';
import 'package:flutter_task1/ui/list_part/character_dto.dart';
import 'package:flutter_task1/generated/l10n.dart';

class CharacterListTile extends StatelessWidget {
  const CharacterListTile({Key? key, required this.character})
      : super(key: key);

  final Character character;

  Color _statusColor(String? status) {
    if (status == "Dead") return AppColors.dead;
    if (status == "Alive") return AppColors.alive;
    return Colors.grey;
  }

  String _statusLabel(String? status) {
    if (status == 'Dead') return S.current.dead;
    if (status == 'Alive') return S.current.alive;
    return S.current.noData;
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: CircleAvatar(
            backgroundImage: AssetImage(AppAssets.images.noAvatar),
            maxRadius: 37,
          ),
        ),
        Expanded(
          child: Column(
            children: [
              Row(
                children: [
                  Expanded(
                    child: Text(
                      _statusLabel(character.status).toUpperCase(),
                      style: AppStyles.s10w500.copyWith(
                        letterSpacing: 1.5,
                        color: _statusColor(character.status),
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Expanded(
                    child: Text(
                      character.name ?? S.of(context).noData,
                      style: AppStyles.s16w500,
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Expanded(
                    child: Text(
                      '${character.species ?? S.of(context).noData}, ${character.gender ?? S.of(context).noData}',
                      style: const TextStyle(
                        color: Colors.grey,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}
