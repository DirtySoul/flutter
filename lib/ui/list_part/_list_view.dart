import 'package:flutter/material.dart';
import 'package:flutter_task1/ui/list_part/character_dto.dart';
import 'package:flutter_task1/ui/list_part/list_tile.dart';

class CharactersListView extends StatelessWidget{
  const CharactersListView({Key? key, required this.charactersList}) : super(key: key);

  final List<Character> charactersList;

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      padding: const EdgeInsets.only(
        top: 12.0,
        left: 12.0,
        right: 12.0,
      ),
      itemCount: charactersList.length,
      itemBuilder: (context, index) {
        return InkWell(
          child: CharacterListTile(character: charactersList[index],),
          onTap: () {},
        );
      },
      separatorBuilder: (context, _) => const SizedBox(height: 26.0),
    );
  }
}