import 'package:flutter/material.dart';
import 'package:flutter_task1/ui/list_part/character_dto.dart';
import 'package:flutter_task1/ui/list_part/grid_tile.dart';

class CharactersGridView extends StatelessWidget {
  const CharactersGridView({
    Key? key,
    required this.charactersList,
  }) : super(key: key);

  final List<Character> charactersList;

  @override
  Widget build(BuildContext context) {
    return GridView.count(
      mainAxisSpacing: 20.0,
      crossAxisSpacing: 8.0,
      childAspectRatio: 0.8,
      crossAxisCount: 2,
      padding: const EdgeInsets.only(
        top: 12.0,
        left: 12.0,
        right: 12.0,
      ),
      children: charactersList.map((person) {
        return InkWell(
          child: CharacterGridTile(person),
          onTap: () {},
        );
      }).toList(),
    );
  }
}
