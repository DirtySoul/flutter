import 'package:flutter_task1/constants/app_colors.dart';
import 'package:flutter_task1/ui/list_part/_list_view.dart';
import 'package:flutter_task1/ui/list_part/_grid_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_task1/constants/app_styles.dart';
import 'package:flutter_task1/ui/list_part/character_dto.dart';
import 'package:flutter_task1/widgets/navigation_bar.dart';
import '../generated/l10n.dart';

class CharacterList extends StatefulWidget {
  const CharacterList({Key? key}) : super(key: key);

  @override
  State<CharacterList> createState() {
    return _CharacterListState();
  }
}

class _CharacterListState extends State<CharacterList> {
  var isListView = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: const BottomNavBar(current: 0),
      body: Column(
        children: [
          SizedBox(height: MediaQuery.of(context).viewPadding.top+10),
          Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Column(
                children: [
                  Container(
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(30.0)),
                      color: AppColors.neutral1,
                    ),
                    child: Row(
                      children: [
                        IconButton(
                            onPressed: () {
                              setState(() {});
                            },
                            icon: const Icon(Icons.search),
                            color: AppColors.neutral2),
                        Expanded(
                          child: TextField(
                            decoration: InputDecoration(
                              hintText: S.of(context).findCharacter,
                              border: InputBorder.none,
                            ),
                          ),
                        ),
                        IconButton(
                          onPressed: () {
                            setState(() {});
                          },
                          icon: const Icon(Icons.filter_list_alt),
                          color: AppColors.neutral2,
                        ),
                      ],
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                          S.of(context)
                              .charactersTotal(_charactersList.length)
                              .toUpperCase(),
                          style: AppStyles.s10w500.copyWith(
                            letterSpacing: 1.5,
                            color: AppColors.neutral2,
                          ),
                        ),
                      IconButton(
                        icon: Icon(isListView ? Icons.list : Icons.grid_view),
                        iconSize: 24.0,
                        color: AppColors.neutral2,
                        onPressed: () {
                          setState(() {
                            isListView = !isListView;
                          });
                        },
                      ),
                    ],
                  ),
                ],
              ),
            ),
          Expanded(
            child: isListView
                ? CharactersListView(charactersList: _charactersList)
                : CharactersGridView(charactersList: _charactersList),
          ),
        ],
      ),
    );
  }
}

final _list = [
  const Character(
    name: 'Рик Санчез',
    species: 'Человек',
    status: 'Alive',
    gender: 'Мужской',
  ),
  const Character(
    name: 'Алан Райс',
    species: 'Человек',
    status: 'Dead',
    gender: 'Мужской',
  ),
  const Character(
    name: 'Саммер Смит',
    species: 'Человек',
    status: 'Alive',
    gender: 'Женский',
  ),
  const Character(
    name: 'Морти Смит',
    species: 'Человек',
    status: 'Alive',
    gender: 'Мужской',
  ),
];

final _charactersList = [
  ..._list,
  ..._list,
  ..._list,
  ..._list,
];
