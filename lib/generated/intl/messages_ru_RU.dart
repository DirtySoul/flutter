// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a ru_RU locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'ru_RU';

  static String m0(total) => "Всего персонажей: ${total}";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "alive": MessageLookupByLibrary.simpleMessage("Живой"),
        "auth": MessageLookupByLibrary.simpleMessage("Авторизация"),
        "characters": MessageLookupByLibrary.simpleMessage("Персонажи"),
        "charactersTotal": m0,
        "close": MessageLookupByLibrary.simpleMessage("Закрыть"),
        "counter": MessageLookupByLibrary.simpleMessage("Счётчик"),
        "counterValue":
            MessageLookupByLibrary.simpleMessage("Значение счётчика"),
        "createAccount": MessageLookupByLibrary.simpleMessage("Создать"),
        "dead": MessageLookupByLibrary.simpleMessage("Мёртвый"),
        "english": MessageLookupByLibrary.simpleMessage("Английский"),
        "error": MessageLookupByLibrary.simpleMessage("Ошибка"),
        "findCharacter":
            MessageLookupByLibrary.simpleMessage("Найти персонажа"),
        "inputErrorLoginIsShort": MessageLookupByLibrary.simpleMessage(
            "Логин должен содержать не менее 3-х символов"),
        "inputErrorPasswordIsShort": MessageLookupByLibrary.simpleMessage(
            "Пароль должен содержать не менее 8 символов"),
        "language": MessageLookupByLibrary.simpleMessage("Язык"),
        "login": MessageLookupByLibrary.simpleMessage("Логин"),
        "loginErrorCheck":
            MessageLookupByLibrary.simpleMessage("Введите логин"),
        "noAccount":
            MessageLookupByLibrary.simpleMessage("У вас ещё нет аккаунта?"),
        "noData": MessageLookupByLibrary.simpleMessage("Нет данных"),
        "password": MessageLookupByLibrary.simpleMessage("Пароль"),
        "passwordErrorCheck":
            MessageLookupByLibrary.simpleMessage("Введите пароль"),
        "russian": MessageLookupByLibrary.simpleMessage("Русский"),
        "settings": MessageLookupByLibrary.simpleMessage("Настройки"),
        "signIn": MessageLookupByLibrary.simpleMessage("Войти"),
        "wrongLoginOrPassword": MessageLookupByLibrary.simpleMessage(
            "Введён неверный логин или пароль")
      };
}
